<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        /**
         * Permissions
         */

        // create and add "manageRoles" permission
        $manageRoles = $auth->createPermission('manageRoles');
        $manageRoles->description = 'User can manage user roles';
        $auth->add($manageRoles);

        // create and add "createItem" permission
        $createItem = $auth->createPermission('createItem');
        $createItem->description = 'User can create an Item';
        $auth->add($createItem);

        // create and add "updateItem" permission
        $updateItem = $auth->createPermission('updateItem');
        $updateItem->description = 'User can update Item';
        $auth->add($updateItem);

        // create and add "deleteItem" permission
        $deleteItem = $auth->createPermission('deleteItem');
        $deleteItem->description = 'User can delete Item';
        $auth->add($deleteItem);

        // create and add "viewItem" permission
        $viewItem = $auth->createPermission('viewItem');
        $viewItem->description = 'User can view Item';
        $auth->add($viewItem);

        /**
         * Roles
         */

        // create and add "regular" role
        $regular = $auth->createRole('regular');
        $auth->add($regular);

        // create and add "admin" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);

        /**
         * Mutual connections
         */

        // "regular" can view and update
        $auth->addChild($regular, $viewItem);
        $auth->addChild($admin, $updateItem);

        // "admin" can do everything what "regular" can
        $auth->addChild($admin, $regular);
        // ... and ...
        // "admin" can delete and create ALL Items and manage roles
        $auth->addChild($admin, $deleteItem);
        $auth->addChild($admin, $createItem);
        $auth->addChild($admin, $manageRoles);
    }
}