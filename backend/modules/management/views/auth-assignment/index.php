<?php

use common\models\User;
use kartik\editable\Editable;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use \backend\modules\management\models\AuthItem;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\management\models\AuthAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auth Assignments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Auth Assignment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'export' => false,
        'pjax' => true,
        'columns' => [
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return \common\models\User::findIdentity($model->user_id)->username;
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'item_name',
                'editableOptions' => function($model) {
                    return
                        [
                            'inputType' => Editable::INPUT_SELECT2,
                            'options' =>
                                [ // your widget settings here
                                    'data' => ArrayHelper::map(AuthItem::find()->where('type = 1')->orderBy(['name' => SORT_ASC])->all(), 'name', 'name'),
                                        //[0=>'0',1=>'1'],
                                    'pluginOptions' =>
                                        [
                                            //'tags' => ['say', 'what'],
                                            //'data' => [0=>'0',1=>'1'],
                                        ],
                                ]
                        ];
                }
            ]
    ],
    ]); ?>

</div>
