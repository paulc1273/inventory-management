<?php

namespace backend\modules\inventory\controllers;

use Yii;
use backend\modules\inventory\models\Item;
use backend\modules\inventory\models\ItemSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (\Yii::$app->user->can('viewItem')) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException('You are not allowed to view items');
        }
    }

    public function actionValidation(){
        $model = new Item();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($model);
        }
        return null;
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (\Yii::$app->user->can('createItem')) {

            $model = new Item();

            if ($model->load(Yii::$app->request->post())) {

                if ($model->imageFile !== null) {
                    $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                    $fileName = 'photos/' . uniqid(rand(), true) . "." . $model->imageFile->extension;
                    $model->imageFile->saveAs($fileName);
                    $model->photo = $fileName;
                }

                $model->created_date = time();
                $model->save();

                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Item has been updated.'));

                return $this->redirect(['view', 'id' => $model->id]);
            } else if (!\Yii::$app->request->isPost) {
                $model->load(Yii::$app->request->get());
                $model->tag_ids = ArrayHelper::map($model->tags, 'name', 'name');
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException('You are not allowed to create items');
        }

    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (\Yii::$app->user->can('updateItem')) {

            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {


                if($model->imageFile !== null) {
                    $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

                    if (!empty($model->imageFile)) {
                        $fileName = 'photos/' . uniqid(rand(), true) . "." . $model->imageFile->extension;
                        $model->imageFile->saveAs($fileName);
                        $model->photo = $fileName;
                    }
                }

                $model->save();

                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Item has been updated.'));

                return $this->redirect(['view', 'id' => $model->id]);
            } else if (!\Yii::$app->request->isPost) {
                $model->load(Yii::$app->request->get());
                $model->tag_ids = ArrayHelper::map($model->tags, 'name', 'name');
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException('You are not allowed to update items');
        }
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (\Yii::$app->user->can('updateItem')) {
            $model = $this->findModel($id);

            unlink($model->photo);

            $model->delete();

            return $this->redirect(['index']);
        } else {
            throw new \yii\web\ForbiddenHttpException('You are not allowed to delete items');
        }
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
