<?php

use backend\modules\inventory\models\Tag;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\inventory\models\ItemTag;

/* @var $this yii\web\View */
/* @var $model backend\modules\inventory\models\Item */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'vendor_id',
                'value'=> $model->vendor->name
            ],
            [
                'attribute' => 'type_id',
                'value'=> $model->type->name
            ],
            'serial_number',
            'price',
            'weight',
            'color',
            'release_date',
            [
                'attribute'=>'photo',
                'value'=>$model->photo,
                'format' => ['image',['height'=>'250']],
            ],
            'created_date',
            [
                'attribute' => 'tags',
                'value' => call_user_func(function ($data) {

                    $tag_query="SELECT (SELECT name FROM tag WHERE id = tag_id) as 'tag' FROM item_tag WHERE item_id =".$data->id;

                    $resultString = "";

                    foreach(Yii::$app->db->createCommand($tag_query)->queryAll() as $tag){
                        $resultString = $resultString.$tag['tag'].', ';
                    }
                    return $resultString;
                }, $model),
            ]
        ],
    ]) ?>


</div>
