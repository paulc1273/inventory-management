<?php

use backend\modules\inventory\models\Tag;
use dosamigos\datepicker\DatePicker;
use kartik\color\ColorInput;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\modules\inventory\models\Vendor;
use backend\modules\inventory\models\Type;

/* @var $this yii\web\View */
/* @var $model backend\modules\inventory\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'enableAjaxValidation' => true,
        'validationUrl' => Url::toRoute('item/validation')
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'vendor_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Vendor::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select Vendor'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'type_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Type::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select Type'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'serial_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true])->hint('ex.: 149.99') ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => true])->hint('ex.: 0.128') ?>

    <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
        'showDefaultPalette' => false,
        'useNative' => false,
        'options' => [
            'placeholder' => 'Select Color',
            'readonly' => true
        ],
        'pluginOptions' => [
            'showInput' => false,
            'showInitial' => false,
            'showPalette' => true,
            'showPaletteOnly' => true,
            'showSelectionPalette' => true,
            'showAlpha' => false,
            'allowEmpty' => false,
            'preferredFormat' => 'name',
            'palette' => [
                [
                    "White", "Black", "Grey", "Silver", "Gold", "Brown",
                ],
                [
                    "Red", "Orange", "Yellow", "Indigo", "Maroon", "Pink"
                ],
                [
                    "Blue", "Green", "Violet", "Cyan", "Magenta", "Purple",
                ],
            ]
        ]
    ]); ?>



    <?= $form->field($model, 'release_date')->widget(
        DatePicker::className(), [
        // inline too, not bad
        'inline' => false,
        // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>

    <?= $form->field($model, 'imageFile')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'initialPreview'=>[!$model->isNewRecord ? $model->photo : ''],
            'initialPreviewAsData'=>true,
            'overwriteInitial'=>true,

            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  'Select Photo'
        ],
    ]);
    ?>

    <fieldset>
        <legend>Tags</legend>
        <?= $form->field($model, 'tag_ids')->widget(Select2::className(), [
            'model' => $model,
            'attribute' => 'tag_ids',
            'data' => ArrayHelper::map(Tag::find()->all(), 'name', 'name'),
            'options' => [
                'multiple' => true,
            ],
            'pluginOptions' => [
                'tags' => true,
            ],
        ]); ?>
    </fieldset>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
