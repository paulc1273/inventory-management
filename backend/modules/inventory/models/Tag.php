<?php

namespace backend\modules\inventory\models;

use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ItemTag[] $itemTags
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemTags()
    {
        return $this->hasMany(ItemTag::className(), ['tag_id' => 'id']);
    }

    public static function getTagByName($name)
    {
        $tag = Tag::find()->where(['name' => $name])->one();
        if (!$tag) {
            $tag = new Tag();
            $tag->name = $name;
            $tag->save(false);
        }
        return $tag;
    }
}
