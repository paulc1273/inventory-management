<?php

namespace backend\modules\inventory\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\inventory\models\Item;

/**
 * ItemSearch represents the model behind the search form about `backend\modules\inventory\models\Item`.
 */
class ItemSearch extends Item
{

    public $globalSearch;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'vendor_id', 'type_id'], 'integer'],
            [['globalSearch', 'name', 'serial_number', 'color', 'release_date', 'photo', 'created_date'], 'safe'],
            [['price', 'weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Item::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->setSort([
            'attributes' => [
                'name',
                'color',
                'vendor_id' => [
                    'asc' => ['vendor.name' => SORT_ASC],
                    'desc' => ['vendor.name' => SORT_DESC]
                ],
                'price',
                'release_date'
            ]
        ]);

        $query->joinWith('vendor');


        $query->orFilterWhere(['like', 'item.name', $this->globalSearch])
            ->orFilterWhere(['like', 'price', $this->price])
            ->orFilterWhere(['like', 'color', $this->globalSearch])
            ->orFilterWhere(['like', 'vendor.name', $this->globalSearch]);

        return $dataProvider;
    }
}
