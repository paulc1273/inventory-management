<?php

namespace backend\modules\inventory\models;

use cornernote\linkall\LinkAllBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\UploadedFile;

/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property string $name
 * @property integer $vendor_id
 * @property integer $type_id
 * @property string $serial_number
 * @property string $price
 * @property string $weight
 * @property string $color
 * @property string $release_date
 * @property string $photo
 * @property string $created_date
 *
 * @property Type $type
 * @property Vendor $vendor
 */
class Item extends \yii\db\ActiveRecord
{

    public $tag_ids;

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            LinkAllBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'vendor_id', 'type_id', 'serial_number', 'price', 'weight', 'color', 'release_date', 'photo'], 'required'],
            [['vendor_id', 'type_id'], 'integer'],
            [['price'], 'number', 'numberPattern' => '/^\d{1,8}(.\d{1,2})?$/', 'message' => 'Price is not valid' ],
            [['weight'], 'number', 'numberPattern' => '/^\d{1,7}(.\d{1,3})?$/', 'message' => 'Weight is not valid'],
            [['imageFile'], 'file'],
            [['release_date', 'created_date', 'tag_ids'], 'safe'],
            [['name', 'serial_number', 'color', 'photo'], 'string', 'max' => 50],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vendor::className(), 'targetAttribute' => ['vendor_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'vendor_id' => 'Vendor',
            'type_id' => 'Type',
            'serial_number' => 'Serial Number',
            'price' => 'Price',
            'weight' => 'Weight',
            'color' => 'Color',
            'release_date' => 'Release Date',
            'photo' => 'Photo',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendor::className(), ['id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function afterSave($insert, $changedAttributes)
    {
        $tags = [];
        foreach ($this->tag_ids as $tag_name) {
            $tag = Tag::getTagByName($tag_name);
            if ($tag) {
                $tags[] = $tag;
            }
        }
        $this->linkAll('tags', $tags);
        parent::afterSave($insert, $changedAttributes);
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('item_tag', ['item_id' => 'id']);
    }
}
