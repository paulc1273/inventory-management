<?php

/* @var $this yii\web\View */

use backend\modules\inventory\models\Item;
use dosamigos\highcharts\HighCharts;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Inventory Management Tool</h1>
    </div>

    <h3>Total items count <b><?= Item::find()->count() ?></b></h3>
    <h3>Average price <b><?= number_format((float)Yii::$app->db->createCommand("SELECT AVG(price) as avg FROM item")->queryOne()['avg'], '2', '.', '')?></b></h3>

    <?php

    /*
     * Item types chart
     */

    $chart_query="SELECT (select type.name FROM type WHERE type.id = item.type_id ) as 'type', COUNT(id) as 'count' FROM item GROUP BY type_id";
    $data = [];

    $allDevicesCount = Item::find()->count();

    foreach (Yii::$app->db->createCommand($chart_query)->queryAll() as $item){
        $data[] = [(string)$item['type'], (int)$item['count'] ];
    }

    echo HighCharts::widget([
        'clientOptions' => [
            'title' => [
                'text' => 'Item Types'
            ],
            'plotOptions' => [
                'pie' =>
                    [
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' =>
                            [
                                'enabled' => true,
                                'format' =>  "<b>{point.name}</b>: {point.percentage:.1f} %",
                                'style' =>
                                    [
                                        "color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'"
                                    ]
                            ]
                    ]
            ],
            'series' => [
                [
                    'type' => 'pie',
                    'name' => 'Count',
                    'colorByPoint' => true,
                    'format' =>  '<b>{point.name}</b>: {point.percentage:.1f} %',
                    'data' => $data
                ]
            ],
        ]
    ]);

    ?>

    <?php

    /*
     * 5 last items grid
     */

    $query = new \yii\db\Query;
    $query->select('
                                id as item_id,
                                (select vendor.name from vendor where vendor.id = vendor_id) as vendor, 
                                (select vendor.logo from vendor where vendor.id = vendor_id) as logo,
                                name,
                                photo,
                                price')
        ->from('item')
        ->orderBy('created_date DESC')
        ->limit(5);

    $provider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => false,
    ]);

    echo GridView::widget([
        'dataProvider' => $provider,
        'summary'=> 'Last added items',
        'columns' => [
            'vendor',
            [
                'attribute' => 'logo',
                'value' => 'logo',
                'format' => ['image', ['height'=>'50']],
            ],
            'name',
            [
                'attribute'=>'photo',
                'value'=>'photo',
                'format' => ['image',['height'=>'50']],
            ],
            'price',
            [
                'attribute' => 'tags',
                'value' => function($model) {

                    $resultString = "";

                    $tag_query = "SELECT (SELECT tag.name FROM tag WHERE tag.id = tag_id) as 'tag' FROM item_tag WHERE item_id =" . $model['item_id'];

                    foreach (Yii::$app->db->createCommand($tag_query)->queryAll() as $tag) {
                        $resultString = $resultString . $tag['tag'] . ', ';
                    }

                    return $resultString;
                }
            ]

        ],
    ]);

    ?>


</div>
