<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use kartik\sidenav\SideNav;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        if (Yii::$app->user->can('manageRoles')){
            $menuItems[] = ['label' => 'User Roles', 'url' => ['/management/auth-assignment']];
        }

        $menuItems[] = ['label' => 'Vendors', 'url' => ['/inventory/vendor']];
        $menuItems[] = ['label' => 'Items', 'url' => ['/inventory/item']];

        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end(); ?>


    <?php if (Yii::$app->getRequest()->getUrl() != Yii::$app->getHomeUrl()){?>
        <div class="sidebar">
            <?php
            $query = new \yii\db\Query;
            $query->select('(select vendor.name from vendor where vendor.id = vendor_id) as vendor, id, name, color')
                ->from('item')
                ->orderBy('created_date DESC')
                ->limit(3);
            $query->createCommand();

            $data = [];

            foreach ($query->createCommand()->queryAll() as $item){
                $value = [];
                $value['label'] = (string)($item['vendor'].' '.$item['name'].' '.$item['color']);
                $value['url'] = 'index.php?r=inventory%2Fitem%2Fview&id='.(string)$item['id'];

                $data[] = $value;
            }

            echo SideNav::widget([
                'type' => SideNav::TYPE_DEFAULT,
                'heading' => 'Last added items',
                'items' => $data,

            ]);
            ?>
        </div>
    <?php }?>



    <div class=<?= Yii::$app->getRequest()->getUrl() != Yii::$app->getHomeUrl() ? "container" : "main-c" ?>>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>

    </div>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
